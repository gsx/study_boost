#include <boost\thread.hpp>

#pragma  once 

namespace space_thead{
	int global_val = 0;
	boost::mutex io_mutex;
	void test_add(){
		boost::mutex::scoped_lock lock(io_mutex); //晕,居然没啥子用..
		for(int i = 0 ; i< 100000;i++)//数字要大一点才好看出来,否则一个线程在时间片内就给加完了.体会不出来
		{
			global_val++;
		}
	}

	void test_syn(){
		boost::thread t1(test_add);
		boost::thread t2(test_add);
		boost::thread t3(test_add);
		t1.join();
		t2.join();
		t3.join();
		printf("after the add ,the val %d",global_val);
	}


}