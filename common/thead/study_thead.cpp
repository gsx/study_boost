#include "stdafx.h"
#include "study_thead.h"
#include "syn_thead\syn_thead.h"
#include "high_level\test_thead.h"



namespace space_thead{



void work_thead()
{
	static int m  = 0;
	printf("work thead %d\n", m++);
}

void work_thead_2param( int m ,int n )
{
	printf("work thead_2param , m:%d ,n is : %d\n",m , n);
}

void work_thead_with_char_str( const char* str )
{
	printf("the str is %s\n",str );
}

int work_thead_ret_value()
{
	return 100;
}

//使用function 包装 函数
void use_function_to_wrapper_thead_func()
{
	boost::function<void () > work_fun(work_thead);
	boost::thread t(work_fun);
	t.join();

	boost::function<void (int ,int) > work_fun2(work_thead_2param);
	boost::thread t2(work_fun2,2,20);
	t2.join();

	boost::function<void  (const char*) > work_fun3(work_thead_with_char_str);
	boost::thread t3(work_fun3 , "this is my param");
	t3.join();
}

void func_no_param_no_ret(){
	printf("func_no_param_no_ret ");
}

void test_thead_func(){
	//不使用function 包装函数,直接在thead 里面初始化

	boost::thread t(&func_no_param_no_ret);
	//t.join();

	boost::thread t2(&work_thead_with_char_str ,"lovleaer");
	//t2.join();

	//测试看不适用函数地址而只是使用名字看能否被调用,,理论上应该是一样的
	boost::thread t3(func_no_param_no_ret);
	//t3.join();

	boost::thread t4(work_thead_2param ,100,200);
	//t4.join();

}

void test()
{
	//use_function_to_wrapper_thead_func();
	//test_thead_func();
	//test_syn();
	space_high_level_thread::test_main();
}

}
