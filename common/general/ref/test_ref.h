#pragma  once 
#include <boost/assign.hpp>
#include <boost/ref.hpp>
#include <boost/typeof/typeof.hpp>//BOOST_AUTO宏
#include <vector>
using namespace boost;
using namespace boost::assign;


namespace space_test_ref{
//该函数是用来测试typedef void result_type的功能，但此处好像并未使用到它
void TestRef1()
{
	struct square 
	{
		//typedef void result_type;//验证改行的意义
		void operator()(int &x)
		{
			x = x * x;
		}
	};

	std::vector<int> v = (list_of(1),2,3,4,5);
	for_each(v.begin(), v.end(), square());

	std::copy(v.begin(),v.end(),std::ostream_iterator<int>(std::cout," "));
	std::cout<<std::endl;
}

class big_class
{
private:
	int x;
public:
	big_class():x(0){}
	void print()
	{
		std::cout<<"big_class "<<++x<<std::endl;
	}
};


template<typename T>
void print(T a)
{
	for (int i = 0;i < 2;++i)
	{
		unwrap_ref(a).print();//解包装
	}
}

void test_main()
{
	TestRef1();
	big_class c;
	BOOST_AUTO(rw,ref(c));
	c.print();//输出1

	print(c);//拷贝传参,输出2,3，内部状态不变
	print(rw);//引用传参，输出2,3， 内部状态改变
	print(c);//拷贝传参，输出4,5，内部状态不变
	c.print();//输出4
}

}