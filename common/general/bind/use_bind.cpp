#include "stdafx.h"
#include "use_bind.h"
#include <iostream>
using namespace std;
#include <boost/bind.hpp>
#include <boost\function.hpp>


namespace space_general_bind{


	int _add(int a  , int b){return a+b ;}

	
	/***************************************
	*author:gsx
	*created on: 2014/01/07
	*purpose :简单的使用boost的实例
	*
	****************************************/
	void test_bind_simple_example(){
		int ans = boost::bind(_add , 10 ,20)();
		printf("the ans %d\n" ,ans );
	}

	struct point  
	{  
		int x, y;  
		point(int a = 9, int b = 9) : x(a), y(b)  {  }  
		void Print()  {  cout << "x value is : " << x << " y value is : " << y << endl;  }  
	};  

		/***************************************
		*purpose :使用函数调用bind 传入的值
		****************************************/
	template<typename T>
	void _call(T& t ){
		printf("use my _call\n");
		t();
	}
	

		/***************************************
		*purpose :如何用一个类来进行延迟调用呢????
		****************************************/
	struct call_class{
		template<class T>
		void operator () (T& t){
			t();
		}
	};

		/***************************************
		*purpose :绑定函数队形,需要 定义 result_type ,这样就能直接用bind .否则要 bind<int> 加一个返回类型
		****************************************/
	struct add_obj{
		typedef int result_type;
		int operator()(int a , int b ){
			return a+b;
		}
	};


	
	
	/***************************************
	*purpose :类成员函数绑定
	****************************************/
	void test_class_member_function_bind(){
		point pt(10 ,22);
			/***************************************
			*purpose :函数名称后面的那个参数必然是实例对象指针
			****************************************/
		  _call(boost::bind(&point::Print , &pt ));
		  //call_class cc ;
		  //cc(boost::bind(&point::Print , &pt ));
		  //cc();
		  

	}

	void test_bind_function_obj(){
		int ans = boost::bind(add_obj() ,10 ,20 )();
		cout<<"ans is "<<ans <<endl;
	}

	void test()
	{
		test_bind_simple_example();
		test_class_member_function_bind();
		test_bind_function_obj();
	}

}

