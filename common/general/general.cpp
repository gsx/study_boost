#include "stdafx.h"
#include "general.h"
#include "ref\test_ref.h"
#include "typeof\test_typeof.h"
#include "bind\use_bind.h"



namespace space_general{ 


int test_func( int a ,int b )
{
	return a+ b;
}

void test_boost_function()
{
	boost::function < int  (int ,int ) > func_obj;
	func_obj = test_func;
	int ret = func_obj(10,20);
	printf("the ret %d \n ", ret);
}

void test_bind_member_function()
{
	boost::function < int  (st_func& ,int ,int ) > func_obj; //格式1 .这里的是对象的引用
	func_obj = &st_func::test_func_sub;//这里必须使用成员变量的指针
	st_func st(20) ;
	int ret = func_obj(st , 10,20);
	printf("the ret %d \n ", ret);


	boost::function < int  (st_func* ,int ,int ) > func_obj2;//格式2. 这里是对象的指针
	func_obj2 = &st_func::test_func_sub;//这里必须使用成员变量的指针
	st_func st2(20) ;
	ret = func_obj2(&st2 , 10,20);
	printf("the ret %d \n ", ret);
}


void test()
{
	//test_boost_function();
	//test_bind_member_function();
	//space_test_ref::test_main();
	//sp_test_typeof::test_typeof_main();
	space_general_bind::test();

}


}