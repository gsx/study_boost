
#pragma once
#include  <boost/function.hpp>

namespace space_general{

	//绑定普通函数
	int test_func(int a ,int b );

	//绑定成员函数
	struct st_func{
		st_func(int v){
			m_v = v;
		}
		int m_v;
		int test_func_sub(int a ,int b) {return a - b + m_v ;}
	};


	//使用 function 功能
	void test_boost_function();

	void test_bind_member_function();

	void test();
}