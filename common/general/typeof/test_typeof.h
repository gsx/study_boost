
#include <vector>
#include <boost/typeof/typeof.hpp> 
#include <string>
namespace sp_test_typeof{
	//学习 BOOST_AUTO
	//BOOST_TYPEOF
	using namespace  std;

	vector<string> func()                           //一个返回vector<string>的函数  
	{  
		vector<string> v(10);  
		return v;  
	}  
	int test_typeof_main()  
	{  
		//*BOOST_AUTO  与 c++ 11 的auto 关键字相同,自动推导类型.但是貌似,不能赋初值
		BOOST_TYPEOF(2.0*3) x = 2.0 * 3;            //推导类型为double  
		BOOST_AUTO(y, 2+3) ;                         //推导类型为int  

		BOOST_AUTO(&a, new double[20]);             //推导类型为double*的引用  
		BOOST_AUTO(p, make_pair(1, "string"));      //推导类型为pair<int,const char*> 

		BOOST_AUTO(v, func());                      //推导类型为vector<string> 
		return 0 ;
	}  

}