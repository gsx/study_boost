
#pragma  once
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
using  namespace boost::asio;

#define  LISTEN_PORT 9987
typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;