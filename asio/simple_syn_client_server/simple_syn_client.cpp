#include "stdafx.h"
#include "simple_syn_client.h"


void space_asio::new_server()
{
	io_service service;
	ip::tcp::endpoint ep( ip::address::from_string("127.0.0.1"), LISTEN_PORT); // listen on 2001
	ip::tcp::acceptor acc(service, ep);
	while ( true) {
		socket_ptr sock(new ip::tcp::socket(service));
		acc.accept(*sock);//会在这里进行阻塞
		printf("the sock remote endpoint address is %s \n" ,sock->remote_endpoint().address().to_string().c_str());
		boost::thread( boost::bind(client_session, sock));//开启匿名线程...
	}
}

void space_asio::client_session( socket_ptr sock )
{
	while ( true) {
		char data[512];
		// 向客户端发送hello world! 
		boost::system::error_code ec; 
		sock->write_some(buffer("hello world!"), ec); 

		// 如果出错，打印出错信息 
		if(ec) 
		{ 
			std::cout << 
				boost::system::system_error(ec).what() << std::endl; 
			break; 
		} 

		size_t len = sock->read_some(buffer(data,512));
		if ( len > 0)
			write(*sock, buffer("ok"));
	}
}


void space_asio::new_client()
{
	io_service service;
	ip::tcp::endpoint ep( ip::address::from_string("127.0.0.1"), LISTEN_PORT);
	ip::tcp::socket sock(service);
	boost::system::error_code ec; 
	sock.connect(ep , ec );//会在这里进行阻塞.
	if( !ec ){
	sock.write_some(buffer("listen adf "));
	char buf[512];
	sock.read_some(buffer(buf ,512));
	printf("client rec :%s\n" , buf ); 
	}else{
		std::cout<<"sock connect error "<< boost::system::system_error(ec).what() <<std::endl;
	}
	sock.close();


}


void space_asio::test_simple_client()
{
	boost::thread t_server(new_server);
	::Sleep(1000*3);
	for(int i = 0 ;i <1 ; ++i){
		boost::thread t_client(new_client);
	}
	t_server.join();
}
