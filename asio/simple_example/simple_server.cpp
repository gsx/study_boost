#include "stdafx.h"
#include "../asio_header.h"
#include "simple_server.h"
#include <boost/thread.hpp>
#include "../simple_syn_client_server/simple_syn_client.h"


namespace space_asio{

	void clientSession::start()
	{
		boost::asio::async_write(m_socket, 
			boost::asio::buffer("link successed!"), 
			boost::bind(&clientSession::handle_write,shared_from_this(), 
			boost::asio::placeholders::error)); 

		/*async_read跟客户端一样，还是不能进入handle_read函数,如果你能找到问题所在，请告诉我，谢谢*/ 
		// --已经解决，boost::asio::async_read(...)读取的字节长度不能大于数据流的长度，否则就会进入 
		// ioservice.run()线程等待，read后面的就不执行了。 
		//boost::asio::async_read(m_socket,boost::asio::buffer(data_,max_len), 
		//         boost::bind(&clientSession::handle_read,shared_from_this(), 
		//         boost::asio::placeholders::error)); 
		//max_len可以换成较小的数字，就会发现async_read_some可以连续接收未收完的数据 
		m_socket.async_read_some(boost::asio::buffer(data_,max_len), 
			boost::bind(&clientSession::handle_read,shared_from_this(), 
			boost::asio::placeholders::error));
	}

	void clientSession::handle_write( const boost::system::error_code& error )
	{
		if(error) 
		{ 
			m_socket.close(); 
		}
	}

	void clientSession::handle_read( const boost::system::error_code& error )
	{
		if(!error) 
		{ 
			std::cout << data_ << std::endl; 
			//boost::asio::async_read(m_socket,boost::asio::buffer(data_,max_len), 
			//     boost::bind(&clientSession::handle_read,shared_from_this(), 
			//     boost::asio::placeholders::error)); 
			m_socket.async_read_some(boost::asio::buffer(data_,max_len), 
				boost::bind(&clientSession::handle_read,shared_from_this(), 
				boost::asio::placeholders::error)); 
		} 
		else 
		{ 
			m_socket.close(); 
		}
	}

	clientSession::~clientSession()
	{

	}

	tcp::socket& clientSession::socket()
	{
		return m_socket;
	}

	clientSession::clientSession( boost::asio::io_service& ioservice ) :m_socket(ioservice)
	{
		memset(data_,'\0',sizeof(data_));
	}


	void serverApp::handle_accept( const boost::system::error_code& error,session_ptr& session )
	{
		if(!error) 
		{ 
			std::cout << "get a new client!" << std::endl; 
			//实现对每个客户端的数据处理 
			session->start(); 
			//在这就应该看出为什么要封session类了吧，每一个session就是一个客户端 
			session_ptr new_session(new clientSession(m_ioservice)); 
			acceptor_.async_accept(new_session->socket(), 
				boost::bind(&serverApp::handle_accept,this,boost::asio::placeholders::error, 
				new_session)); 
		}
	}

	serverApp::serverApp( boost::asio::io_service& ioservice, tcp::endpoint& endpoint ) :m_ioservice(ioservice), 
		acceptor_(ioservice, endpoint)
	{
		session_ptr new_session(new clientSession(ioservice)); 
		acceptor_.async_accept(new_session->socket(), 
			boost::bind(&serverApp::handle_accept,this,boost::asio::placeholders::error, 
			new_session));
	}

	serverApp::~serverApp()
	{

	}

	void open_server()
	{
		boost::asio::io_service myIoService; 
		short port = LISTEN_PORT/*argv[1]*/; 
		//我们用的是inet4 
		tcp::endpoint endPoint(tcp::v4(), port); 
		//终端（可以看作sockaddr_in）完成后，就要accept了 
		serverApp sa(myIoService, endPoint); 
		//数据收发逻辑 
		myIoService.run();
	}

	int simple_asyn_test()
	{
		boost::thread  t_server(open_server);
		Sleep(3*1000);
		boost::thread t_client(new_client);
		return 0; 
	}
}


