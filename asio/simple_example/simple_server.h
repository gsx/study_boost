// boost.cpp : 定义控制台应用程序的入口点。 
// 
//-

//myfileinfo
/********************************************************************
	created:	2014/01/02
	created:	2:1:2014   23:19
	author:		gsx
	purpose:	从网上抄的一个简单的异步asio 的封装.
*********************************************************************/


#pragma  once 
#include <boost/asio.hpp> 
#include <boost/bind.hpp> 
#include <boost/shared_ptr.hpp> 
#include <boost/enable_shared_from_this.hpp> 
#include <iostream> 

using boost::asio::ip::tcp; 

namespace space_asio{
#define max_len 1024

class clientSession : public boost::enable_shared_from_this<clientSession> 
{ 
public: 
	clientSession(boost::asio::io_service& ioservice);
	~clientSession();
	tcp::socket& socket(); 
	void start(); 
private: 
	void handle_write(const boost::system::error_code& error); 
	void handle_read(const boost::system::error_code& error); 
private: 
	tcp::socket m_socket; 
	char data_[max_len]; 
}; 

class serverApp 
{ 
	typedef boost::shared_ptr<clientSession> session_ptr; 
public: 
	serverApp(boost::asio::io_service& ioservice, tcp::endpoint& endpoint); 
	~serverApp(); 
private: 
	void handle_accept(const boost::system::error_code& error,session_ptr& session); 
private: 
	boost::asio::io_service& m_ioservice; 
	tcp::acceptor acceptor_; 
}; 

int simple_asyn_test();

}