/*游程编码又称“运行长度编码”或“行程编码”，是一种统计编码，该编码属于无损压缩编码。对于二值图有效。 
　　行程编码的基本原理是：用一个符号值或串长代替具有相同值的连续符号（连续符号构成了一段连续的“行程”。行程编码因此而得名），使符号长度少于原始数据的长度。 
　　例如：5555557777733322221111111 
　　行程编码为：（5，6）（7，5）（3，3）（2，4）（l，7）。可见，行程编码的位数远远少于原始字符串的位数。 
　　并不是所有的行程编码都远远少于原始字符串的位数，但行程编码也成为了一种压缩工具。
　　例如：555555 是6个字符 而（5，6）是5个字符，这也存在压缩量的问题，自然也会出现其他方式的压缩工具。
　　在对图像数据进行编码时，沿一定方向排列的具有相同灰度值的像素可看成是连续符号，用字串代替这些连续符号，可大幅度减少数据量。 
　　行程编码分为定长行程编码和不定长行程编码两种类型。 
　　行程编码是连续精确的编码，在传输过程中，如果其中一位符号发生错误，即可影响整个编码序列，使行程编码无法还原回原始数据。
    以上引自：http://baike.baidu.com/view/721796.htm
	*/

/*
  transfer aaaa -> a3
           abab -> abab
           abbbd -> ab3d
  */
namespace  test_practise_small{
#include <string>
#include <list>
#include <memory.h>
#include <string.h>
using namespace std;
char* encode(char* src)
{
    char* dest=src;
    int count;
    if(src)
    {
        while(*dest=*src)
        {
            count = 1;
            while(*(++src)==*dest)
                count++;
            if(count > 1)
            {
                sprintf(dest+1,"%d",count);
                while(*(++dest))
                    ;
            }else
                dest++;
        }
    }
    return src;
}

void test_encode(void)
{
    list<string> str;
    str.push_back("aaaaaaa");
    str.push_back("abbbd");
    str.push_back("555555777773332222    1111111");
    str.push_back("abab");
    str.push_back("");
    str.push_back("abbbdeddddddddddeeeeeec");
    str.push_back("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    char* p = (char*)malloc(1000);

    cout << "================test_encode============"<<endl;
    for(list<string>::iterator i=str.begin(); i!=str.end();i++)
    {
        strcpy(p, i->c_str());
        cout<< p <<" -> ";
        encode(p);
        cout<< p << endl;
    }
    free(p);
}

}